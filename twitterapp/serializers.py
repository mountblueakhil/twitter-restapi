from rest_framework import serializers
from .models import Profilemodel,User,Tweetmodel


class UserSerializer(serializers.ModelSerializer):
    class Meta :
        model =User
        fields = '__all__'

class ProfileSerializer(serializers.ModelSerializer):
   
    class Meta :
        model =Profilemodel
        fields = '__all__'


class TweetSerializer(serializers.ModelSerializer):
    class Meta :
        model =Tweetmodel
        fields = '__all__'