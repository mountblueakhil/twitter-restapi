from rest_framework import viewsets
from . import models,serializers


class UserViewset(viewsets.ModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
    

class ProfileViewset(viewsets.ModelViewSet):
    queryset = models.Profilemodel.objects.all()
    serializer_class = serializers.ProfileSerializer
    
class TweetViewset(viewsets.ModelViewSet):
    queryset = models.Tweetmodel.objects.all()
    serializer_class = serializers.TweetSerializer
    