from django.db import models
# from django.contrib.auth.models import User




# Create your models here.
class User(models.Model) :
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    password = models.CharField(max_length=200) 
    username=models.CharField(max_length=200,blank=True)

class Profilemodel(models.Model):
    image = models.FileField(upload_to="ProfilePics")
    bio = models.CharField(max_length=200)
    followers = models.ManyToManyField(User,related_name='followers',blank=True,null=True)
    following  = models.ManyToManyField(User,blank=True,null=True)
    user = models.OneToOneField(User,on_delete=models.CASCADE,related_name="user")

class Tweetmodel(models.Model):
    image = models.FileField(upload_to="ProfilePics",blank=True)
    desc = models.CharField(max_length=200)
    time = models.DateTimeField(auto_now_add=True, blank=True)
    profileuser = models.ForeignKey(Profilemodel,related_name="profile",on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)