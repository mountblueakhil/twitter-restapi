from twitterapp.viewsets import ProfileViewset ,UserViewset,TweetViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('profiles',ProfileViewset)
router.register('user',UserViewset)
router.register('tweet',TweetViewset)
